#!/usr/bin/env python3
# GNU General Public License v3.0

def insertion_sort(arr): #vezme kazdy nasledujici, da vlevo a porovna a posouva; n^2
	for i in range(1,len(arr)):
		j=i
		while arr[j-1] > arr[j] and j>0:
			arr[j-1],arr[j]=arr[j],arr[j-1]
			j-=1
	return arr

def selection_sort(arr): #hleda nejmensi zprava, pak vlozi vlevo; n^2, ale trochu pomalejsi jak insertion
	for i in range(len(arr)-1):
		idx=i
		for j in range(i+1,len(arr)):
			if arr[j]<arr[idx]:
				idx=j
		arr[i],arr[idx]=arr[idx],arr[i]
	return arr

def merge_sort(arr): #rozdeluje na nejmensi pulky, pak je porovnava a sklada; n*log(n)
	if len(arr)>1:
		left_arr=arr[:len(arr)//2]
		right_arr=arr[len(arr)//2:]
		merge_sort(left_arr)
		merge_sort(right_arr)
		left_idx=0
		right_idx=0
		merge_idx=0
		while left_idx < len(left_arr) and right_idx < len(right_arr):
			if left_arr[left_idx] < right_arr[right_idx]:
				arr[merge_idx]=left_arr[left_idx]
				left_idx+=1
			else:
				arr[merge_idx]=right_arr[right_idx]
				right_idx+=1
			merge_idx+=1
		while left_idx<len(left_arr):
			arr[merge_idx]=left_arr[left_idx]
			left_idx+=1
			merge_idx+=1
		while right_idx<len(right_arr):
			arr[merge_idx]=right_arr[right_idx]
			right_idx+=1
			merge_idx+=1
	return arr

def quick_sort(arr,left,right): #;n*log(n) avg, worst n^2
	def partition():
		left_idx=left
		right_idx=right-1
		pivot=arr[right]
		while left_idx<right_idx:
			while left_idx<right and arr[left_idx]<pivot:
				left_idx+=1
			while right_idx>left and arr[right_idx]>=pivot:
				right_idx-=1
			if left_idx<right_idx:
				arr[left_idx],arr[right_idx]=arr[right_idx],arr[left_idx]
		if arr[left_idx]>pivot:
			arr[left_idx],arr[right]=arr[right],arr[left_idx]
		return left_idx
	if left<right:
		partition_pos=partition()
		quick_sort(arr,left,partition_pos-1)
		quick_sort(arr,partition_pos+1,right)
	return arr