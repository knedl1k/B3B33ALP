#method find, returns -1 if its not in the array
def find(where,what):
    for start in range (len(where)+len(what)+1):
        i=0
        while i<len(what) and where[start+i]==what[i]:
            i+=1
        if(i==len(what)):
            return start
    return -1
if __name__ == "__main__":
    print(find([3,4,5,6],[5]))
