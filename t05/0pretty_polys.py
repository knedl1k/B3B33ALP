#sum,multiply and print pretty two polys from input
def sum(poly1, poly2):
    sum = [0] * max(len(poly1), len(poly2))
    for i in range(len(poly1)):
        sum[i] += poly1[i]
    for i in range(len(poly2)):
        sum[i] += poly2[i]
    return sum
def multiply(poly1, poly2):
    res = [0] * (len(poly1) + len(poly2))
    for i in range(len(poly1)):
        for j in range(len(poly2)):
            res[i + j] += poly1[i] * poly2[j]
    return res
def print_out(poly):  # prints the polynom
    first = 0
    for i in range(len(poly)):
        symb = '+'
        if (poly[i] == 0):
            continue
        if (poly[i] < 0 or i == 0 or first == 0):
            symb = ''
        if (i == 0):
            print(str(symb) + str(poly[i]) + "x", end="")
            first = 1
        else:
            print(str(symb) + str(poly[i]) + "x^" + str(i), end="")
            first = 1
    return None
if __name__ == "__main__":
    poly1 = list(map(int, input("Zadej polynom:").split()))
    poly2 = list(map(int, input("Zadej polynom:").split()))
    poly = sum(poly1, poly2)
    print_out(poly)
    print()
    poly = multiply(poly1, poly2)
    print_out(poly)
    print()