#rekurze


def binom(n, k):
    if(k==0 or k == n):
        return 1
    if(k==1 or k==n-1):
        return n
    return binom(n - 1, k) + binom(n - 1, k - 1)

if __name__ == "__main__":
    print(binom(0,0))

