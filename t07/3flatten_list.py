#narovnani

def flatten(input_list,output_list):
    for item in input_list:
        if (type(item)==list):
            flatten(item,output_list) #vezme si podlist a vlozi ho do "nadlistu" nad nim
        else:
            output_list.append(item) #prvky na stejne urovni proste ulozi do pole
    return None

if __name__ == "__main__":
    a=[1,2,3,[4,5,[6,7]]]
    b=[]
    flatten(a,b)
    print(a)
    print(b)