#Fibonacciho posloupnost

def fibo(size):
    if (size==0 or size==1):
        known[0]=1
        known[1]=1
        return 1
    if(size in known):
        return known[size]
    value = fibo(size - 2) + fibo(size - 1)
    known[size]=value
    return value

if __name__ == "__main__":
    size=60000
    known= {} #datovy typ dictionary
    for i in range(size):
        print(fibo(i)," ",end="")
#print(known)