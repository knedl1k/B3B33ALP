#nasobeni matice a matice

def matrix_print(matrix):
    for row in range(len(matrix)):
        for col in range(len(matrix[0])):
            print(matrix[row][col], end=" ")
        print()
    return None

def matrix_multipl(a,b):
    if(len(a[0])!=len(b) or not(len(a)>0 and len(b))): #ochrana pred prazdnymi maticemi nebo
        return []
    res=[[0]*len(b[0]) for _ in range(len(a))]
    for row in range(len(a)):
        for col in range(len(b[0])):
            tmp=0
            for pos in range(len(a[0])): #pocet sloupcu v prvni a pocet radku v druhe musi byt stejny, takze nezalezi na kterou zafixuji
                tmp+=a[row][pos]*b[pos][col]
            res[row][col]=tmp #optimalizace, zmensim komunikaci procesoru s ramkou
    return res

if __name__ == "__main__":
    a = [[1,1],
         [1,1],
         [2,3],
         [7,5]
         ]
    b = [[0,1],
         [-1,0],
        ]

    matrix_print(matrix_multipl(a,b))