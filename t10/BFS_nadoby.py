# prohledavani stavovyho prostoru
# prelevani nadob
# BFS = breath

VOLUME = [5, 8, 3]  # objemy nadob

class State:
    def __init__(self, pocat_vekt, previous=None):  # constructor
        self.vekt = pocat_vekt[:]  # uplna kopie pole
        self.action = ""
        self.previous = previous
    def expand(self):  # result's array of State, metoda = funkce
        res = []
        for x in range(len(self.vekt)):  # Nalej x
            if (self.vekt[x] < VOLUME[x]):
                new = State(self.vekt, self)  # vytvoreni noveho uzlu
                new.vekt[x] = VOLUME[x]  # naleje maximum do daneho kalichu
                new.action = "N" + str(x)
                res.append(new)
        for x in range(len(self.vekt)):  # Vylej x
            if (self.vekt[x] > 0):
                new = State(self.vekt, self)  # vytvoreni noveho uzlu
                new.vekt[x] = 0  # vylej maximum z daneho kalichu
                new.action = "V" + str(x)
                res.append(new)
        for x in range(len(self.vekt)):  # prelevani xPy
            for y in range(len(self.vekt)):
                if (x == y) or (self.vekt[x] == 0) or (self.vekt[y] == VOLUME[y]):
                    continue
                new = State(self.vekt, self)
                zbytekY = VOLUME[y] - self.vekt[y]
                if self.vekt[x] >= zbytekY:
                    new.vekt[x] -= zbytekY
                    new.vekt[y] += zbytekY
                else:
                    new.vekt[y] += new.vekt[x]
                    new.vekt[x] = 0
                new.action = str(x) + "P" + str(y)
                res.append(new)
        return res

if __name__ == "__main__":
    pocat_vekt = [3, 2, 0]
    start = State(pocat_vekt)
    open_list = [start]
    is_known = {}
    while (len(open_list) > 0):
        actual = open_list.pop(0)
        if actual.vekt == [1, 2, 3]:
            print("Solution:")
            tmp = actual
            while (tmp != None):
                print(tmp.vekt, tmp.action)
                tmp = tmp.previous
            quit()
        is_known[str(actual.vekt)] = 1
        new_nodes = actual.expand()
        for node in new_nodes:
            if str(node.vekt) not in is_known:
                open_list.append(node)
    print("NO SOLUTION")
