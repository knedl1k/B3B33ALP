#flood fill - vyplní jen ty místa, kam to může přetéct

def pm(matrix):
    for rows in range(len(matrix)):
        for cols in range(len(matrix[rows])):
            print(matrix[rows][cols], end="")
        print()
    print()
    return None

def inside_check(matrix, row, col):
    return (row >=0 and row<len(matrix) and col>=0 and col<len(matrix[0]))

def floodfill(matrix, row, col):
    stack=[]
    stack.append([row,col]) #souradnice pocatecniho bodu
    neighbors=[[-1,0],[1,0],[0,1],[0,-1]] #relativni indexy sousedu
    while(len(stack)>0):
        active=stack.pop()
        arow,acol=active
        matrix[arow][acol]= '*'
        for n in neighbors:
            roffset,coffset=n
            row=arow+roffset
            col=acol+coffset
            if(inside_check(matrix, row, col) and matrix[row][col]==0):
                stack.append([row,col])
    return matrix

if __name__ == "__main__":
    #čtyřokolí
    #zásobník - ukládám si to, co musim ještě dodělat
    matrix = [
        [0, 0, 1, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 1, 1, 0, 1, 0, 0, 0, 1],
        [0, 0, 1, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 1, 0, 0],
        [0, 0, 1, 1, 0, 1, 0, 0, 0, 0],
        [0, 0, 1, 0, 1, 1, 1, 1, 0, 0],
        [0, 0, 1, 0, 0, 1, 0, 1, 1, 1],
        [0, 0, 1, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 1, 0, 0, 0, 0]
        ]
    pm(floodfill(matrix, 0, 9))
