#inverzni permutace k permutaci
def printPerm(label, permu):
    str=""
    for i in range(len(permu)):
        str+=label[permu[i]]
    return str

def printPerm2(label,permu):
    return "".join([label[x] for x in permu])

def inverz(permu):
    inv_permu=[0]*len(permu)
    for i in range(len(permu)):
        inv_permu[permu[i]]=i
    return inv_permu

if __name__ == "__main__":
    label="ahoj"
    permu=[3,2,0,1]
    label=printPerm(label, permu)
    print(label)
    permu=inverz(permu)
    label=printPerm(label,permu)
    print(label)
    permu = inverz(permu)
    label=printPerm2(label,permu)
    print(label)
    permu = inverz(permu)
    label = printPerm2(label, permu)
    print(label)