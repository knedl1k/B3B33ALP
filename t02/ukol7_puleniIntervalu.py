#metoda puleni intervalu = bisection method
#f(x)=x**2-y
y=float(input())
L=0
R=y
while(abs(L-R)>1e-3):
    M=(L+R)/2
    Lv=L**2-y
    Rv=R**2-y
    Mv=M**2-y
    if(Rv>0 and Mv>=0) or (Rv<0 and Mv<=0):
        R=M
    elif(Lv<0 and Mv<0) or (Lv>0 and Mv>0):
        L=M
print("Střed:",M,"Vstup:",y,"Střed na druhou:",M*M)