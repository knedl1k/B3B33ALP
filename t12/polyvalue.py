import sys

def funkce(x,y):
    return (1/2)*x**2 * (1-y)**2 + (x-2)**3 - 2*y +x
    
def funkce2(x,y):
    return funkce(x,y)*(x+2)*(y-2)


x=(list(map(float, input().split())))
y=(list(map(float, input().split())))
if(len(x)!=len(y)):
    print("ERROR")
    quit()
vysledky=[]
for i in range(len(x)):
    vysledky.append(funkce(x[i],y[i]))
nejvetsi=0
pocet_nejmensi=0
nejvetsi_index=0
for i in range(len(vysledky)):
    if(vysledky[i]>nejvetsi):
        nejvetsi=vysledky[i]
        nejvetsi_index=i
    if(vysledky[i]==nejvetsi):
        if(nejvetsi_index>i):
            nejvetsi_index=i
    if(vysledky[i]<0):
        pocet_nejmensi+=1
    
minimum=[]
for i in range(len(x)):
    minimum.append(funkce2(x[i],y[i]))
nejmensi=minimum[0]
nejmensi_index=0
for i in range(len(minimum)):
    if(minimum[i]<nejmensi):
        nejmensi=minimum[i]
        nejmensi_index=i
    if(minimum[i]==nejmensi):
        if(nejmensi_index>i):
            nejmensi_index=i
                
print(nejvetsi_index,pocet_nejmensi,nejmensi_index)