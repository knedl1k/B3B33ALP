# min heap
# bubble up
import sys


class Heap:
    def __init__(self):
        self.arr = []

    def add(self, value):
        self.arr.append(value)
        self.bubble_up(len(self.arr)-1)

    def bubble_up(self, child):
        while (child > 0):
            parent = (child-1)//2
            if (self.arr[parent] > self.arr[child]):
                # vymena parent a child
                self.arr[parent], self.arr[child] = self.arr[child], self.arr[parent]
            else:
                break
            child = parent

    def top(self):
        if (len(self.arr) == 0):
            return None
        if (len(self.arr) == 1):
            return self.arr.pop()
        value = self.arr[0]
        self.arr[0] = self.arr.pop()
        self.bubble_down(0)
        return value

    def bubble_down(self, parent):
        while (parent != -1):
            left = parent*2+1
            right = parent*2+2
            if (left < len(self.arr)):
                tmp = -1
                if (self.arr[parent] > self.arr[left]):
                    tmp = left
                if (right < len(self.arr) and self.arr[right] < self.arr[left]):
                    tmp = right
                if tmp != -1:
                    self.arr[parent], self.arr[tmp] = self.arr[tmp], self.arr[parent]
                parent = tmp
            else:
                break

if __name__ == '__main__':
    h = Heap()
    h.add(6)
    h.add(-1)
    h.add(-2)
    h.add(5)
    h.add(-7)
    #v=h.top()
    #while v!=None:
    #    print()
    #    v=h.top()
    print(h.arr)
