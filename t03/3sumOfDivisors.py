import sys
def sumOfDivisors(n):
    s=0
    #print(n)
    for i in range (1,n+1):
        if n%i==0:
            s+=1
    return s

if __name__=="__main__":
    for n in range (1,1000):
        if n == sumOfDivisors(n):
            print(n)