def shift(letter,n):
    if not(letter.isalpha()):
        return letter
    letter2=ord(letter)+n
    if (letter.isupper()):
        if(letter2>ord('Z')):
            letter2-=26
        elif(letter2<ord('A')):
            letter2+=26
    else:
        if(letter2>ord('z')):
            letter2-=26
        elif(letter2<ord('a')):
            letter2+=26
    return chr(letter2)
def caesar(string,n):
    result=''
    for letter in string:
        new=shift(letter,n)
        result+=new
    return result
print(caesar('FEL',4))