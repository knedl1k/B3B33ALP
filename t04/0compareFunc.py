#compares these two static functions just to prove they're different
def a(x, y, z):
    return (x and y) or (not y and z)
def b(x, y, z):
    return x or z
if __name__ == "__main__":
    for i in range(2):
        for j in range(2):
            for k in range(2):
                if(a(i,j,k)!=b(i,j,k)):
                    print("Functions are different!")
                    quit()
