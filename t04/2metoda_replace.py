#metoda replace
def find(where,what):
    for start in range (len(where)+len(what)+1):
        i=0
        while i<len(what) and where[start+i]==what[i]:
            i+=1
        if(i==len(what)):
            return start
    return -1
def replace(where, what, new):
    start=find(where,what)
    if start==-1:
        return where
    return where[:start] + new + where[start+len(what):]
if __name__ == "__main__":