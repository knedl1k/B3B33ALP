#finds the second biggest number in the given list of nums
def maximum(nums):
    if len(nums) == 0:
        return None, -1
    else:
        maxi = nums[0]
        inx = 0
        for i in range(len(nums)):
            if (maxi < nums[i]):
                maxi = nums[i]
                inx = i
        return maxi, inx


def minimum(nums):
    if len(nums) == 0:
        return None, -1
    else:
        mini = nums[0]
        inx = 0
        for i in range(len(nums)):
            if mini > nums[i]:
                mini = nums[i]
                inx = i
        return mini, inx


def minimum2(nums):
    mini1 = minimum(nums)
    i = 0
    while (i < len(nums) and nums[i] == mini1):
        i += 1
    if (i == len(nums)):
        print("Second minimum is not present.")
        return None
    mini2 = nums[i]
    for i in range(len(nums)):
        if nums[i] != mini1:
            if nums[i] < mini2:
                mini2 = nums[i]
    return mini2


def maximum2(nums):
    maxi1 = maximum(nums)
    i = 0
    while (i < len(nums) and nums[i] == maxi1):
        i += 1
    if (i == len(nums)):
        print("Second maximum is not present.")
        return None
    maxi2 = nums[i]
    for i in range(len(nums)):
        if nums[i] != maxi1:
            if nums[i] < maxi2:
                maxi2 = nums[i]
    return maxi2


if __name__ == "__main__":
    array = list(map(int, input().split()))
    maxi = maximum2(array)
    print("The second biggest number is", str(maxi))
    mini = minimum2(array)
    print("The second smallest number is", str(mini))
