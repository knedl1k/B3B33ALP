# prints pretty polynomials
def polynomy(poly):  # prints the polynom
    first = 0
    for i in range(len(poly)):
        symb = '+'
        if (poly[i] == 0):
            continue
        if (poly[i] < 0 or i == 0 or first == 0):
            symb = ''
        if (i == 0):
            print(str(symb) + str(poly[i]) + "x", end="")
            first = 1
        else:
            print(str(symb) + str(poly[i]) + "x^" + str(i), end="")
            first = 1
    return None
def calc_sum(poly, x):  # calculates polynom's value
    sum = 0
    for i in range(len(poly)):
        sum += poly[i] * x ** i
    return sum
def minValue(poly, a, b):  # finds the minimum value in the polynom function
    if (a < b):
        min = calc_sum(poly, a)
        while a < b:
            if calc_sum(poly, a) < min:
                min = calc_sum(poly, a)
            a += 0.01
    else:
        min = calc_sum(poly, b)
        while (b < a):
            if calc_sum(poly, b) < min:
                min = calc_sum(poly, b)
            b += 0.01
    return min
if __name__ == "__main__":
    poly = list(map(int, input("Zadej polynom:").split()))
    a = int(input("Zadej a:"))
    b = int(input("Zadej b:"))
    # x = int(input("Zadej X:"))
    polynomy(poly)
    print()
    # print("Sum:", calc_sum(poly, x))
    print(minValue(poly, a, b))
