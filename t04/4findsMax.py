#finds max value from the given input list of nums
def maximum(nums):
    if len(nums) == 0:
        return None, -1
    else:
        maxi = nums[0]
        inx = 0
        for i in range(len(nums)):
            if (maxi < nums[i]):
                maxi = nums[i]
                inx = i
        return maxi, inx
def minimum(nums):
    if len(nums) == 0:
        return None, -1
    else:
        mini = nums[0]
        inx = 0
        for i in range(len(nums)):
            if mini > nums[i]:
                mini = nums[i]
                inx = i
        return mini, inx
if __name__ == "__main__":
    array = list(map(int, input().split()))
    maxi, index = maximum(array)
    print("The biggest number is", str(maxi), "on the", str(index) + ". position.")
    mini, index = minimum(array)
    print("The smallest number is", str(mini), "on the", str(index) + ". position.")
