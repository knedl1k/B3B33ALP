#!/usr/bin/env python3
import sys
def load_matrix(filename):  # loads and stores input matrix
    matrix = []
    f = open(filename, "r")
    R,S=list(map(int,f.readline().split()))
    print(R,S)
    for r in range(R):
    	matrix.append(list(map(int,f.readline().split())))
    print(matrix)
    P=[]
    for line in f:
    	values=list(map(int,line.split()))
    	piece=[]
    	for i in range(len(values)//2):
    		piece.append([values[2*i+0],values[2*i+1]])
    	P.append(piece)
    #print(P)
    P2=[]
    for piece in P:
    	newP=[]
    	for cell in piece:
    		r,c=cell
    		r-=piece[0][0]
    		c-=piece[0][1]
    		newP.append([r,c])
    	P2.append(newP)
    return P2,matrix
def inside(r,c,m):
	return r>=0 and r<len(matrix) and c>=0 and c<len(matrix[r])

def canBePlaced(piece,matrix,r,c):
	for cell in piece:
		r1,c1=cell
		r1+=r
		c1+=c
		if inside(r1,c1,matrix) and matrix[r1][c1]==0:
			pass
		else:
			return False
	return True

def rotate(piece,rot):


	return piece

def solve(n,P2,matrix):
	if n==len(P2):
		isValid=True
		for r in range(len(m)):
			for c in range(len(m[r])):
				if matrix[r][c]==0:
					isValid=False
		if isValid:
			print("reseni:",matrix)
			quit()

	for r in range(len(matrix)):
		for c in range(len(matrix[r])):
			for rot in range(4):
				rotp=rotate(P2[n],rot)
				if canBePlaced(rotp,matrix,r,c):
					for cell in rotp:
						r1,c1=cell
						matrix[r1+r][c1+c]=n+1
					solve(n+1,P2,matrix)
					for cell in rotp:
						r1,c1=cell
						matrix[r1+r][c1+c]=n+1

if __name__=="__main__":
	P2,matrix=load_matrix("ubo.txt")
	solve(0,P2,matrix)
