#!/usr/bin/env python3
import sys

#eval()
def myEval(s):
	signs=[]
	for char in s:
		if char=="+" or char=="*":
			signs.append(char)
	s2=s.replace("+"," ").replace("*"," ")
	value=s2.split()
	news=""
	for i in range(len(value)):
		news+=str(int(value[i]))
		if i < len(signs):
			news+=signs[i]
	return eval(news)

def solve(pre,post):
	print(pre,post)
	for i in range(1,len(pre)):
		p1=pre[:i]
		p2=pre[i:]
		for sign in ["+","*"]:
			new=p1+sign+p2
			r1=0
			if len(post[1:])>0:
				r1=myEval(post[1:])
			if r1>n and (not "0" in pre[:i]):
				continue

			value=myEval(new+post)
			if value==n:
				print("Reseni:",new+post)
				sys.exit()
			solve(p1,sign+p2+post)
	return None


if __name__=="__main__":
	digits="1208"
	n=3
	solve(digits,"")
	print("NO SOLUTION")
