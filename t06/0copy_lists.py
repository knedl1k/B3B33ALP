import copy
if __name__ == "__main__":
    a = []  # vytvoř pole
    R=3
    C=4
    #a=[[0]*C]*R #kopíruju reference, když změnim jednu souřadnici v jednom podpoli, změní se ve všech ostatních
    #for r in range(R):
        #row=[0]*C
        #for c in range(C):
        #    row.append(0)
        #a.append(row)

    a=[[0]*C for _ in range(R)] #podtržítko je proměnná, ale chci dát najevo, že ji nepoužívám
    b = a[:] #shallow copy
    c=copy.deepcopy(a) #kompletní kopie pole bez referencí
    a[0][1]=2
    #b[1]='*' #vyhodi mi to podpole a nahradi ho '*' pouze v b
    b[1][0]=3 #nahradi mi to znak na pozici [1][0] '*' v b i a

    
    print("a:",a)
    print("b:",b)
    print("c:",c)