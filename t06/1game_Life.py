#hra Life
#osmi okolí
import copy
import time
def pm(matrix): #print matrix
    for r in range(len(matrix)):
        for c in range(len(matrix[r])):
            if (matrix[r][c]==1):
                print("*",end="")
            else:
                print(".",end="")
        print()
    return None
def rules(matrix,new_matrix,r,c,living_n):
    new_matrix[r][c]=0 #vsechny nove zabiju a budu postupne ozivovat
    if (matrix[r][c]==1 and (living_n==2 or living_n==3)): #pokud stara zije a zaroven ma prave 2 nebo prave 3 sousedy, zustane na zivu
        new_matrix[r][c]=1
    if (matrix[r][c]==0 and living_n==3): #pokud je stara mrtva a ma 3 sousedy, ozije
        new_matrix[r][c]=1
    return new_matrix
def life_step(matrix): #výpočet počtu živých v okolí jiné živé buňky
    new_matrix=copy.deepcopy(matrix)
    for r in range(len(matrix)):
        for c in range(len(matrix[r])):
            living_n=0 #zijici sousedi
            for r_o in [-1,0,1]: #row offset
                for c_o in [-1,0,1]: #column offset
                    #souradnice je r+r_o, c+c_o
                    if (r_o==0 and c_o==0): #abych nepocital samotny prvek, ze ma sam sebe za souseda
                        continue
                    if(matrix[(r+r_o)%len(matrix)][(c+c_o)%len(matrix[0])] == 1): #modulo kvuli tomu, ze mi to mohlo pretect, diky modulu to bude nekonecny
                        living_n+=1
            rules(matrix,new_matrix,r,c,living_n)               
    return new_matrix



if __name__ == "__main__":
    matrix=[
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 1, 1, 1, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
     [0, 0, 0, 1, 1, 0, 0, 0, 0, 0],
     [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
   ]
    while True:
        matrix=life_step(matrix)
        pm(matrix)
        print()
        time.sleep(1)