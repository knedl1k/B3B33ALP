#!/usr/bin/env python3
# GNU General Public License v3.0
import sys
import copy
def walls(floor,matrix):
    for i in range(len(matrix)):
        beg_x,beg_y=matrix[i][0],matrix[i][1]
        end_x,end_y=matrix[i][2],matrix[i][3]
        if beg_x!=end_x: #up-down
            shift=beg_x
            if beg_x<=end_x:
                while shift<=end_x:
                    floor[shift][beg_y]=1
                    shift+=1
            else:
                while shift>=end_x:
                    floor[shift][beg_y]=1
                    shift-=1
        elif beg_y!=end_y: #right-left
            shift=beg_y
            if beg_y<=end_y:
                while shift<=end_y:
                    floor[beg_x][shift]=1
                    shift+=1
            else:
                while shift>=end_y:
                    floor[beg_x][shift]=1
                    shift-=1
        else: #single cube 1x1
            floor[beg_x][beg_y]=1
    return floor
def load_matrix(filename): #loads input data; matrix stores data about walls
    matrix=[]
    with open(filename,"r") as file:
        num_row,num_col=list(map(int,file.readline().strip().split()))
        floor=[[0]*num_col for i in range(num_row)]
        for line in file:
            matrix.append(list(map(int,line.split())))
    floor=walls(floor,matrix) #stores walls into the floor
    return floor
def inside_check(floor,row,col): #checks boarders of the floor
    return row >= 0 and row<len(floor) and col >= 0 and col<len(floor[0])
def floodfill(floor,row,col,num):
    stack=[]
    stack.append([row,col]) #souradnice pocatecniho bodu
    neighbors=[[-1,0],[1,0],[0,1],[0,-1]] #relativni indexy sousedu
    while len(stack)>0:
        active=stack.pop()
        arow,acol=active
        floor[arow][acol]=num
        for n in neighbors:
            roffset,coffset=n
            row=arow+roffset
            col=acol+coffset
            if inside_check(floor,row,col) and floor[row][col] == 0:
                stack.append([row,col])
    return floor
def area(floor,num): #returns sum of the area filled with the number
    sum=0
    for i in range(len(floor)):
        sum+=floor[i].count(num)
    return sum
if __name__ == "__main__":
    filename=(sys.argv[1] if len(sys.argv)>1 else "/home/knedl1k/Desktop/B3B33ALP/ZK/ZK02.txt")
    floor=load_matrix(filename) #2D list of floor with walls
    smallest=len(floor)*len(floor[0]) #stores the biggest possible area for the first check
    num=2 #number to fill the area with
    for row in range(len(floor)):
        for col in range(len(floor[row])):
            if floor[row][col] == 0:
                floor=floodfill(floor,row,col,num) #use floodfill to fill out all possible gaps
                new_area=area(floor,num) #calculates the newly filled area
                if smallest>new_area: #if finds smaller area,stores its value
                    smallest=new_area
                num+=1 #every iteration changes filling number to optimize the speeds
    print(smallest)
