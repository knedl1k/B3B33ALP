#!/usr/bin/env python3
# GNU General Public License v3.0
import sys
def to_string(arr):
    return "".join(arr)
def load_words(filename):  # stores matrix from file into 2D array
    words=[]
    with open(filename,"r") as file:
        for line in file:
            words.append(list(map(str,line.strip())))
    new_words=[]
    for word in words:
        new_words.append(to_string(word))
    return new_words
def find_longest(words): #find the longest word from the input
    longest=len(words[0])
    for i in range(1, len(words)):
        if longest<len(words[i]):
            longest=len(words[i])
    return longest
def calculate(words):
    diff_arr=[0]*(find_longest(words)+1)
    for i in range(len(words)): #leading word
        word=words[i]
        for j in range(i+1,len(words)): #following words
            if len(word)!=len(words[j]): #compares only words of equal length
                continue
            letter,diff=0,0
            while(letter<len(word)):
                if word[letter]!=words[j][letter]: #if words differentiate in letter, +1 to diffs
                    diff+=1
                letter+=1 #next letter
            diff_arr[diff]+=1
    return diff_arr
def pprint(diff_arr): #pretty print to fulfill the expected output
    for i in range(len(diff_arr)):
        if diff_arr[i]==0:
            continue
        print(i, diff_arr[i])
if __name__=="__main__":
    filename=sys.argv[1] if len(sys.argv) > 1 else "/home/knedl1k/Desktop/B3B33ALP/ZK/ZK01.txt"
    words=load_words(filename) #input words
    diff_arr=calculate(words) #array of differences
    pprint(diff_arr) #print the output
