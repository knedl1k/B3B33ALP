#!/usr/bin/env python3
# GNU General Public License v3.0
import sys
def kill(): #kills the program and prints ERROR
    print("ERROR")
    exit()
def get_seconds(time): #recalculates inputted time into seconds
    if time.index("h")!=2 or time.index("m")!=5 or time.index("s")!=8:
        kill()
    hours=int(time[0])*10+int(time[1])
    mins=int(time[3])*10+int(time[4])
    secs=int(time[6])*10+int(time[7])
    if hours>99 or mins>59 or secs>59:
        kill()
    hours=hours*60*60
    mins=mins*60
    return hours+mins+secs
def get_difference(time): #calculates difference between two times
    secs=str(time%60)
    mins=str((time//60)%60)
    hours=str((time//60)//60)
    if len(secs)<2:
        secs="".join("0"+secs)
    if len(mins)<2:
        mins="".join("0"+mins)
    if len(hours)<2:
        hours="".join("0"+hours)
    return hours,mins,secs
def valid_input(time): #checks if the input is valid
    return "h" in time and "m" in time and "s" in time or time[0] in ["0","1","2","3","4","5","6","7","8","9"]
if __name__=="__main__":
    first=str(input())
    second=str(input())
    if not (valid_input(first) and valid_input(second)):
        kill()
    first=get_seconds(first)
    second=get_seconds(second)
    diff=abs(first-second)
    hours,mins,secs=get_difference(diff)
    print(hours+"h"+mins+"m"+secs+"s") #pretty print