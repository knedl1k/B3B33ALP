#!/usr/bin/env python3
# GNU General Public License v3.0
import sys
def load_matrix(filename): # stores matrix from file into 2D array
    matrix=[]
    with open(filename,"r") as f:
        for line in f:
            matrix.append(list(map(int,line.split())))
    return matrix
def reset():
    return 0,0,0
def storing(sum,longest,idxr,idxc,coordr,coordc):
    if sum>longest:
        longest=sum
        coordr=idxr
        coordc=idxc
    return longest,coordr,coordc
def find_longest(matrix,enc):
    longest=0
    coordr,coordc=-1,-1
    for row in range (len(matrix)):
        sum,idxr,idxc=reset()
        for col in range (enc):
            if matrix[row][col]%2==0 and matrix[row][col]>=0:
                if sum==0:
                    idxr=row
                    idxc=col
                sum+=1
            else:
                longest,coordr,coordc=storing(sum,longest,idxr,idxc,coordr,coordc)
                sum,idxr,idxc=reset()
        longest,coordr,coordc=storing(sum,longest,idxr,idxc,coordr,coordc)
    return longest,coordr,coordc
if __name__=="__main__":
    filename=sys.argv[1] if len(sys.argv)>1 else "/home/knedl1k/Desktop/B3B33ALP/preparation_exam/sequence.txt"
    matrix=load_matrix(filename)
    for enc in range(2):
        if enc==0:
            longestR,coordrR,coordcR=find_longest(matrix,len(matrix[0]))
        else:
            longestC,coordrC,coordcC=find_longest(matrix,len(matrix))
    if longestR==longestC==0:
        print("NEEXISTUJE")
        exit()
    if longestR>longestC:
        print("r",coordrR,coordcR,longestR)
    else:
        print("s",coordrC,coordcC,longestC)
