#!/usr/bin/env python3
# GNU General Public License v3.0
import sys
def load_matrix(filename): #stores matrix from file into 2D array
    matrix=[]
    words=[]
    with open(filename,"r") as f:
        for line in f:
            matrix.append(list(map(str,line.strip())))
    for i in range(len(matrix)): #converts chars into substings
        words.append("".join(matrix[i])) 
    return words
def find_substing(matrix): #finds the biggest substring in the whole file
    lenght=-1
    substring=None
    for king in range (len(matrix[0])):
        letter=matrix[0][king]
        for i in range(1,len(matrix)):
            if not letter in matrix[i]:
                break
        else:
            for second in range(king+1,len(matrix[0])):
                letter+=matrix[0][second]
                for i in range(1,len(matrix)):
                    if not letter in matrix[i]:
                        letter=letter[:-1]
                        if lenght<len(letter):
                            lenght=len(letter)
                            substring=letter
                        break
    return substring

if __name__=="__main__":
    filename=sys.argv[1] if len(sys.argv)>1 else "/home/knedl1k/Desktop/B3B33ALP/preparation_exam/subwords.txt"
    matrix=load_matrix(filename)
    substring=find_substing(matrix)
    if substring==None: 
        print("NEEXISTUJE") #there's no suitable substing
    else:
        print(substring) #prints the biggest substring in the whole file