#!/usr/bin/env python3
# GNU General Public License v3.0
import sys
def load_words(filename):  #stores matrix from file into 2D array
    words=[]
    new=[]
    with open(filename,"r") as f:
        for line in f:
            words.append(list(map(str,line.strip())))
    for word in words:
        new.append("".join(word))
    return new
if __name__=="__main__":
    filename=sys.argv[1] if len(sys.argv) > 1 else "/home/knedl1k/Desktop/B3B33ALP/preparation_exam/postfix.txt"
    suffix=sys.argv[2] if len(sys.argv) > 1 else "S"
    words=load_words(filename)
    count=0
    shortest=None
    for i in range(len(words)):
        for j in range(0, len(suffix)):
            if not words[i][-1-j] == suffix[-1-j]:
                break
        else:
            count+=1
            if shortest==None:
                shortest=words[i]
            elif len(words[i])<len(shortest):
                shortest=words[i]
    print(count)
    print(shortest)