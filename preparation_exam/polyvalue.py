#!/usr/bin/env python3
# GNU General Public License v3.0
import sys
def function1(x,y):
    return (((1/2)*x**2)*((1-y)**2))+((x-2)**3)-(2*y)+x
def function2(x,y):
    return function1(x,y)*(x+2)*(y-2)
if __name__=="__main__":
    input_x=list(map(float,input().split()))
    input_y=list(map(float,input().split()))
    if len(input_x)!=len(input_y):
        print("ERROR")
        exit()
    value=function1(input_x[0],input_y[0])
    biggest=value
    idx_big=0
    negative_count=0
    if value<0:
        negative_count += 1
    for i in range(1,len(input_y)):
        value=function1(input_x[i],input_y[i])
        if value>biggest:
            idx_big=i
            biggest=value
        elif value==biggest:
            if idx_big>i:
                idx_big=i
        if value<0:
            negative_count+=1
    value=function2(input_x[0],input_y[0])
    lowest=value
    idx_low=0
    for i in range(1,len(input_y)):
        value=function2(input_x[i],input_y[i])
        if value<lowest:
            idx_low=i
            lowest=value
        elif value == biggest:
            if idx_low>i:
                idx_low=i
    print(idx_big,negative_count,idx_low)
"""
0. 1. -1. 0.5
1. 0. -1. -1.5
"""